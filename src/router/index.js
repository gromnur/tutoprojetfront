import Vue from 'vue'
import Router from 'vue-router'

import Personne from "../components/Personne";
import Groupe from "../components/Groupe";
import Accueil from "../components/Accueil";
import SinglePersonne from "../components/SinglePersonne";

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Accueil
    },
    {
      path: '/personne',
      component: Personne
    },
    {
      path: '/personne/:id',
      component: SinglePersonne,
      name: 'singlepersonne',
    },
    {
      path: '/groupe',
      component: Groupe
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
